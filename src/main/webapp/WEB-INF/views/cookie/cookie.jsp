ex<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$("#cooking").click(function(){
			cookieJson();
       	});
		
		//listCookie();
	});
	
	function cookieJson(){
		var name = "test" ;
		var value  = "${pageContext.request.requestURL}" ;
		alert(name);
		
		$.ajax({
			type:"post",
			url:"addCookie.do",
			headers:{
				"content-type" : "application/json",
				"accept" : "UTF-8"
				},
			dateType: "json",
			//param형식보다 편하다.
			data:JSON.stringify({
				name : name,
				value : value
			}),
			success: function(data){
				alert(value);
			}
		});
	}

    function setCookie(cName, cValue, cDay){
        var expire = new Date();
        expire.setDate(expire.getDate() + cDay);
        cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
        if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
        document.cookie = cookies;
    }
 
    function getCookie(cName) {
        cName = cName + '=';
        var cookieData = document.cookie;
        var start = cookieData.indexOf(cName);
        var cValue = '';
        if(start != -1){
            start += cName.length;
            var end = cookieData.indexOf(';', start);
            if(end == -1)end = cookieData.length;
            cValue = cookieData.substring(start, end);
        }
        return unescape(cValue);
    }
    
    //목록으로 띄우기 - REST
    function listCookie(){
    	$.ajax({
    		type : "get",
    		url : "/getCookie.do",
    		success : function(result){
    			$("#cookieList").html(result);
    		}
    	})
    }
    
    

</script>
</head>
<body>
	<h1>
		Hello world!  
	</h1>
	<P>  The time on the server is ${serverTime}. </P>

	<button type="button" id="cooking">JAVA로 쿠키생성</button>
	<br><br><br><br>
	<input type="button" value="쿠키 생성" onclick="setCookie('nono', 'EnglishEnglish', 1)">
	<input type="button" value="쿠키 보기" onclick="alert(getCookie('nono'))">
	<input type="button" value="쿠키 삭제" onclick="setCookie('nono', '', -1)">

	
</body>
</html>


 
