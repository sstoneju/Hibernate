<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<script src="https://cdn.ckeditor.com/ckeditor5/10.0.0/classic/ckeditor.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
		editor();
		
        $("#btnSave").click(function(){
        	//var content = CKEDITOR.instances.content.editor.getData();
            var content = $("#content").val();
            var writer = $("#title").val();
            if(title == ""){
                alert("제목을 입력하세요");
                document.form1.title.focus();
                return;
            }
            if(content == ""){
                alert("내용을 입력하세요");
                document.form1.content.focus();
                return;
            }
            document.form1.submit();
        });
    });


    function editor(){
    	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			console.log( editor );
		} )
		.catch( error => {
			console.error( error );
		} );
	}
    
	</script>
	<title>WRITE</title>
</head>
<body>

<form name="form1" method="post" action="/board/insert.do">
	<div style="width: 800px; margin: auto;">
		제목: 
        <input name="title" id="title" size="80" placeholder="제목을 입력해주세요">
        <br><br>
		<textarea name="content" id="editor"></textarea>
		<br>
		<button type="button" id="btnSave">확인</button>
	</div>
</form>
</body>
</html>