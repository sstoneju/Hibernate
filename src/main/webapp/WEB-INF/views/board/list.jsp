<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#btnWrite").click(function(){
				//페이지 주소 변경(이동)
				location.href = "write.do";
				})
			})
	</script>
	
	
</head>
<body>
	<div style="margin:auto; padding:30px">
		<a href='#'><span>Home</span></a>
		<a href='#'><span>About</span></a>
		<a href='#'><span>Services</span></a>
		<a href='#'><span>Contact</span></a>
	</div>
	<div style="margin:auto; padding:20px">
		<table border="1px" width="800">
		<colgroup>
	           <col width="80%"/>
	           <col width="10%"/>
	           <col width="10%"/>
		</colgroup>
		<tr>
			<th scope="col">제목</th>
			<th scope="col">작성자</th>
			<th scope="col">조회수</th>
		</tr>
		<c:forEach var="row" items="${boards }">
			<tr>
				<td>
					${row.title }
				</td>
				<td>
				
				<c:if test ="${empty row.userName  }">
					${row.userName }
				</c:if>

				</td>
				<td>
				<c:if test ="${empty row.rdcnt  }">
					0
				</c:if>
				<c:if test ="${!empty row.rdcnt  }">
					${row.rdcnt }
				</c:if>
				</td>
				
			</tr>
		</c:forEach>
		</table>	
	</div>
	<div>
		<button type="button" id="btnWrite">글쓰기</button>
	</div>
</body>
</html>

 
