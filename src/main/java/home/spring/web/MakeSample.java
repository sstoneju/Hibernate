package home.spring.web;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import home.spring.web.board.dto.BoardVO;

public class MakeSample {
	
	SessionFactory factory = HibernateUtil.getSessionFactory();
	
	 public void make() {
		 BoardVO vo = new BoardVO();
	 }
	 
	 public void insert(BoardVO vo) {
			Session session = factory.getCurrentSession();
			session.beginTransaction(); // 트렌잭션 시작
			session.save(vo);
			session.getTransaction().commit(); // 트랜젝션 커밋
		}
}
