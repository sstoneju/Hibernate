package home.spring.web;

import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import home.spring.web.board.dto.SiteVO;

@Controller
@RequestMapping("/cookie/*")
public class CookieController {
	private static final Logger logger = Logger.getLogger(CookieController.class);

	/**
	 * 좋아 JSON으로 Controller에 파일을 넘겨주는 것까지는 잘 성공했다.
	 * 이제 생성된 쿠키들을 뿌려주면 된다.
	 * 
	 */
	@RequestMapping("/cookie.do")
	public String cookie() {
		return "cookie/cookie";
	}
	
	
	@RequestMapping(value ="/addCookie.do", method = RequestMethod.POST, consumes = {"application/json"})
	public void addCookie(@RequestBody SiteVO vo, HttpServletResponse response){		
		logger.info(vo.getName()+" : "+vo.getValue());
		Cookie setCookie = new Cookie(vo.getName(), vo.getValue());
		setCookie.setMaxAge(60*60*24*7); // 기간을 지정
		response.addCookie(setCookie);
		logger.info("받아라 받아야 받아");
	}

	
	//쿠키 가져오기
	@RequestMapping(value="/getCookie.do", method=RequestMethod.GET)
	public ModelAndView getCookie(HttpServletRequest request, ModelAndView mav) throws Exception{
		Cookie[] cookie = request.getCookies();
			if(cookie != null){
				for(Cookie c : cookie){
					String name = URLDecoder.decode(c.getName(), "UTF-8"); // 쿠키 이름 가져오기
					String value = URLDecoder.decode(c.getValue(), "UTF-8"); // 쿠키 값 가져오기
					logger.info("Ajax로 실행: "+name+" - "+value);
					mav.addObject(name, value);
				}
			}
		mav.setViewName("cookie/listCookie");
		return mav;
	}
	
	
	
	//특정 쿠키 제거
	//@RequestMapping(value="/", method=RequestMethod.GET)
	public String testCookie(HttpServletResponse response){
		Cookie kc = new Cookie("choiceCookieName", null); // choiceCookieName(쿠키 이름)에 대한 값을 null로 지정
		kc.setMaxAge(0); // 유효시간을 0으로 설정
		response.addCookie(kc); // 응답 헤더에 추가해서 없어지도록 함
		
		return "";
	}

	//모든 쿠키 제거
	//@RequestMapping(value="/", method=RequestMethod.GET)
	public String testCookie(HttpServletRequest request, HttpServletResponse response){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(int i=0; i< cookies.length; i++){
				cookies[i].setMaxAge(0); // 유효시간을 0으로 설정
				response.addCookie(cookies[i]); // 응답 헤더에 추가
			}
		}
		
		return "";
	}
}
