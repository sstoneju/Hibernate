package home.spring.web;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mail/*")
public class MailController {
	
	
	@Autowired
	private JavaMailSender mailSender;
	
	@RequestMapping("/mailForm")
	public String mailForm() {
		return "mail/mailForm";
	}
	
	@RequestMapping("/mailSending")
	public String mailSending(HttpServletRequest request) {
		String setform = "sstoneju@ecplaza.net";
		String tomail = request.getParameter("tomail");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			
			messageHelper.setFrom(setform);
			messageHelper.setTo(tomail);
			messageHelper.setSubject(title);
			messageHelper.setText(content);
			
			mailSender.send(message);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "mail/mailForm";
	}
	
	@RequestMapping("/simpleMailSending")
	public String simpleMailSending(HttpServletRequest request) {
		String setform = "sstoneju@ecplaza.net";
		String tomail = request.getParameter("tomail");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		try {
		
			SimpleMailMessage Message = new SimpleMailMessage();
			Message.setFrom(setform);
			Message.setTo(tomail);
			Message.setSubject(title);
			Message.setText(content);
			
			mailSender.send(Message);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "mail/mailForm";
	}
}
