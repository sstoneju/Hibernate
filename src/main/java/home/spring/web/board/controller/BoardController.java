package home.spring.web.board.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import home.spring.web.HomeController;
import home.spring.web.board.dto.BoardVO;
import home.spring.web.board.service.BoardService;

@Controller
@RequestMapping("/board/*")
public class BoardController {
	
	@Autowired
	BoardService boardService;
	
	private static final Logger logger = Logger.getLogger(HomeController.class);
	private static final int BID = 1;
	
	@RequestMapping("/list.do")
	public String boardList(@ModelAttribute BoardVO vo, Model model) throws Exception{
		model.addAttribute("boards", boardService.boardList());
		return "board/list";
	}
	
	@RequestMapping("/write.do")
	public String boardWrite(@ModelAttribute BoardVO vo) {
		return "board/write";
	}
	
	@RequestMapping("/insert.do")
	public String mboardInsert(@ModelAttribute BoardVO vo, HttpSession session, Model model) {
		
		vo.setbId(BID); // BID를 저장해 준다.
		if(session.getAttribute("userId") != null & session.getAttribute("userName")!= null) {
			String userId = (String) session.getAttribute("userId");
			String userName = (String) session.getAttribute("userName");
		}
				
		boardService.boardInsert(vo);
		logger.info(vo.getTitle());
		logger.info(vo.getContent());
		return "board/list";
	}
	
	/*@RequestMapping("detail.do")
	public String mboardDetail(@ModelAttribute MboardVO vo, Model model) throws Exception{
		MboardVO rsVO = new MboardVO();
		rsVO = mboardService.boardDetail(vo);
		//model.addAttribute("board", rsVO);
		return "detail";
	}
	
	
	
	*/
	
}
