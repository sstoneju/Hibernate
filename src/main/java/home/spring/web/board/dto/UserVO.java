package home.spring.web.board.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Users")
public class UserVO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userSeq;
	
	private String userId;
	private String userName;
	private String userEmail;
	private Date brithDay;
	
	public UserVO() {
		
	}
	
	public long getUserSeq() {
		return userSeq;
	}
	public void setUserSeq(long userSeq) {
		this.userSeq = userSeq;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public Date getBrithDay() {
		return brithDay;
	}
	public void setBrithDay(Date brithDay) {
		this.brithDay = brithDay;
	}
	public UserVO(String userId, String userName, String userEmail, Date brithDay) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.brithDay = brithDay;
	}
	
	
	
	
}
