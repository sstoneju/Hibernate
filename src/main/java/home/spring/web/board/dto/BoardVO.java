package home.spring.web.board.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity(name="BoardEntity")
@Table(name = "Board")
public class BoardVO implements Serializable{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bId;
    
    private String title;
    private String content;
    private String userName;
    private String viewCnt;
    @ManyToOne
    @JoinColumn(name="userId")
    private UserVO uesrVO;
    private String show;
    
    public BoardVO() {
    	
    }
    
	public BoardVO(int bId, String title, String content, String userName, String viewCnt, UserVO uesrVO,
			String show) {
		super();
		this.bId = bId;
		this.title = title;
		this.content = content;
		this.userName = userName;
		this.viewCnt = viewCnt;
		this.uesrVO = uesrVO;
		this.show = show;
	}

	
	@Override
	public String toString() {
		return "MboardVO [bId=" + bId + ", title=" + title + ", content=" + content + ", userName="
				+ userName + ", viewCnt=" + viewCnt + ", uesrVO=" + uesrVO + ", show=" + show + "]";
	}

	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getViewCnt() {
		return viewCnt;
	}
	public void setViewCnt(String viewCnt) {
		this.viewCnt = viewCnt;
	}
	public String getShow() {
		return show;
	}
	public void setShow(String show) {
		this.show = show;
	}
	public UserVO getUesrVO() {
		return uesrVO;
	}
	public void setUesrVO(UserVO uesrVO) {
		this.uesrVO = uesrVO;
	}

	public long getbId() {
		return bId;
	}

	public void setbId(long bId) {
		this.bId = bId;
	}
	
	
	
	
}
