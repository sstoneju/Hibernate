package home.spring.web.board.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import home.spring.web.board.dao.BoardDAO;
import home.spring.web.board.dto.BoardVO;

@Service
public class BoardServiceImpl implements BoardService {
	
	@Autowired
	BoardDAO boardDAO;
	
	@Override
	public List<BoardVO> boardList() {
		return boardDAO.boardList();
	}

	@Override
	public void boardInsert(BoardVO vo) {
		boardDAO.boardInsert(vo);
		
	}

}
