package home.spring.web.board.service;

import java.util.List;

import home.spring.web.board.dto.BoardVO;

public interface BoardService {
	public List<BoardVO> boardList();
	//public MboardVO boardDetail(MboardVO vo);
	public void boardInsert(BoardVO vo);
}
