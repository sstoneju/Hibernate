package home.spring.web.board.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import home.spring.web.HibernateUtil;
import home.spring.web.board.dto.BoardVO;


@Repository
public class BoardDAOImpl implements BoardDAO {

	SessionFactory factory = HibernateUtil.getSessionFactory();

	@Override
	public List<BoardVO> boardList() {
		Session session = factory.getCurrentSession();
		session.beginTransaction(); // 트렌잭션 시작
		Query query = session.createQuery("from "+"BoardVO");
        List<?> list = query.list();
		session.getTransaction().commit(); // 트랜젝션 커밋
		return null;
	}
	
	@Override
	public void boardInsert(BoardVO vo) {
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		session.save(vo);
		session.getTransaction();
		
	}

}
