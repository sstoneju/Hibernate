package home.spring.web.board.dao;

import java.util.List;

import home.spring.web.board.dto.UserVO;

public interface UserDAO {
	public UserVO selectUser(int userId);
	public List<UserVO> userList();
	
}
