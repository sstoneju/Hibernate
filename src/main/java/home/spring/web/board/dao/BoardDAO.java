package home.spring.web.board.dao;

import java.util.List;

import home.spring.web.board.dto.BoardVO;

public interface BoardDAO {
	public List<BoardVO> boardList();
	public void boardInsert(BoardVO vo);
}
