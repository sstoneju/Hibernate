---------------------------------------05/01
create table boardVO(
    BID integer,
    BNO integer primary key,
    TITLE varchar2(200),
    CONTENT varchar2(4000),
    USER_NAME varchar2(20),
    VIEWCNT integer,
    USER_ID varchar2(20),
    SHOW varchar2(1) default 'Y');
    
create table sh_user(
    USER_ID varchar2(20) primary KEY,
    USER_PW varchar2(30),
    USER_EMAIL varchar2(20),
    USER_NAME varchar2(20),
    REG_DATE DATE default SYSDATE not null,
    UPDATE_DATE DATE);

    